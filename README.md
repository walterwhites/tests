# tests

We use codeception framework to run acceptance / functionnal tests,
to get more informations, go here https://codeception.com/quickstart

To resume:

1) Page Object are class of the page with CSS selector and commun actions.

2) Steps Object are Actor class like Admin, Client, Visitor and actions associated
(Because Admin doesn't have same methods of classic User)

3) Cest class are our class to run the test


Our stack:

1) We use WebDriver module to run acceptance tests

2) We use testing bot to run tests in cloud

3) We also use docker to run on local containers (look docker-compose.yml)


Notes:

When we run with our free TestingBot account, we use SSH, to connect using tunnel run:

java -jar testingbot-tunnel.jar key secret

it's there to get them: https://testingbot.com/members/user/edit