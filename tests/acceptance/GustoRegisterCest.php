<?php


use Page\HomePage;

class GustoRegisterCest
{
    /** @var HomePage */
    private $homepage;

    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function RegisterClient(AcceptanceTester $I)
    {
        $this->homepage = $I->visit(HomePage::class);
        $this->homepage->register(getenv('FIRSTNAME'), getenv('LASTNAME'),  getenv('NEW_USER_EMAIL'), getenv('NEW_USER_EMAIL'));
        $this->homepage->signIn(getenv('NEW_USER_EMAIL'), getenv('NEW_USER_EMAIL'));
    }
}
