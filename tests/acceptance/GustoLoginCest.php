<?php


use Page\HomePage;

class GustoLoginCest
{
    /** @var HomePage */
    private $homepage;

    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function loginClient(AcceptanceTester $I)
    {
        $this->homepage = $I->visit(HomePage::class);
        $this->homepage->signIn(getenv('EMAIL'), getenv('PASSWORD'));
    }
}
