<?php


use Page\HomePage;
use Step\Acceptance\Client;

class GustoHomeCest
{
    /** @var HomePage */
    private $homepage;

    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function homeIsVisible(Client $C)
    {
        $C->amOnPage('/');
        $this->homepage = $C->visit(Homepage::class);
        $this->homepage->pageIsDisplayed();
    }
}
