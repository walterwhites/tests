<?php
namespace Page;
abstract class BasePage
{
    protected $tester;
    protected $headerPage;
    public $URL;

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;
        $this->headerPage = new HeaderPage($I);
    }
    public function url(...$args)
    {
        if ($args)
            return sprintf($this->URL, ...$args);
        else
            return str_replace('%s', '', $this->URL);
    }
}