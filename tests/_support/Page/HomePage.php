<?php
namespace Page;

class HomePage extends BasePage
{
    // include url of current page
    public $URL = '/';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public function route($param)
    {
        return $this->URL.$param;
    }

    public function visit() {
        $this->tester->amOnPage($this->URL);
        return $this;
    }

    public function signIn($username, $password) {
        $this->tester->click(".modal-trigger");
        $this->tester->waitForElementVisible("#gusto_login_email", 10);
        $this->tester->fillField("#gusto_login_email", $username);
        $this->tester->fillField("#gusto_login_password", $password);
        $this->tester->click("Se connecter");
        $this->tester->waitForText("Tableau de bord");
        $this->tester->waitForText("Mon email: " . $username);
        $this->tester->seeInCurrentUrl('/dashboard');
    }

    public function register($firstname, $lastname, $username, $password) {
        $this->tester->click(".modal-trigger");
        $this->tester->click("Inscription");
        $this->tester->waitForElementVisible("#gusto_register_email", 10);
        $this->tester->fillField("#gusto_register_firstname", $firstname);
        $this->tester->fillField("#gusto_register_lastname", $lastname);
        $this->tester->fillField("#gusto_register_email", $username);
        $this->tester->fillField("#gusto_register_password", $password);
        $this->tester->click("//label[@for=\"gusto_register_termsAccepted\"]");
        $this->tester->click("S'inscrire");
    }

    public function pageIsDisplayed() {
        $this->tester->waitForText("Un espace de coworking");
        $this->tester->waitForText("Nous proposons des réservation d'espaces de coworking dans notre formidable café à Gare du Nord");
    }
}
