<?php

use Page\HomePage;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    protected $user;
    public $page;

    public function visit($pageObject, ...$args)
    {
        $page = new $pageObject($this);
        $this->amOnPage($page->url(...$args));
        $this->on($page);
        return $this->page;
    }

    public function on($page)
    {
        if (is_string($page) && class_exists($page))
        {
            $page = new $page($this);
        }
        $this->page = $page;
        return $this->page;
    }

    public function __call($method, $parameters)
    {
        if ($this->page && method_exists($this->page, $method)) {
            $this->page->{$method}(...$parameters);
            return $this;
        }
        parent::__call($method, $parameters);
        return $this;
    }

    public function getCurrentUrl() {
        return $this->executeJS("return location.href");
    }
}
