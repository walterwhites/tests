<?php
namespace Step\Acceptance;

use Page\HomePage;

class Client extends \AcceptanceTester
{
    /** @var  HomePage */
    protected $homePage;

    protected function _inject (HomePage $homePage)
    {
        $this->homePage = $homePage;
    }

    public function login()
    {
        $this->homePage->clickOnSignIn();
    }

    public function book()
    {
        $I = $this;
    }

}